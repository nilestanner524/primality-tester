﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace fermat
{
    public partial class Form1 : Form
    {

        private Random randomizer = new Random((int)DateTime.Now.Ticks);
        public Form1()
        {
            InitializeComponent();
        }
        // total complexity: O(n) + O(k^3) + O(kn^3) + O(2^k) + 3O(1)
        // because k << n
        // O(kn^3) or O(n^3)
        private void solve_Click(object sender, EventArgs e)
        {
            HashSet<BigInteger> As = new HashSet<BigInteger>();
            BigInteger inputNum = Convert.ToInt32(input.Text);
            BigInteger inputK = Convert.ToInt32(k.Text);
            bool isPrime = true;
            string outputText;

            if (inputK > inputNum) // O(n) if the comparison is a subtraction
            {
                inputK = inputNum; // k must be equal to or smaller than the prime to check
            }
            for (int i = 0; i < inputK; i++) // O(k^3) kn for ++, kn for the loop, and kn for the getRandom function 
            {
                As.Add(getRandom(inputNum, As)); //get k random numbers
            }
            //O(kn^3) because the modexp function is repeated k times
            foreach (int a in As) // for each a value
            {
                // check if a^p-1 mod p is equal to 1
                // && the result with isPrime to keep track of whether the inputNum is prime
                isPrime = isPrime && modexp(a, inputNum - 1, inputNum) == 1;
            }
            if (isPrime) //O(1)
            {
                //the total error of using this method is 1/2^k or 2^-k
                // 1 minus the total error would give us the probability of whether the entered number is prime
                outputText = "yes with probability " + (1 - Math.Pow(2, -(int)inputK)).ToString(); // O(2^k)

            }
            else {
                outputText = "no"; //O(1)
            }
            output.Text = outputText;//O(1)
        }

        //According to the book, this algorithm takes O(n^3) (p.19) for n possible recursion calls with O(n^2) multiplication at each call
        private BigInteger modexp(BigInteger x, BigInteger y, BigInteger n)
        {
            if (y == 0) //base case
            {
                return 1; // if y is 0 then any number^0 would be 1
            }
            BigInteger modtwo = y % 2; //find the remainder of y mod 2
            BigInteger yfloored = (y - modtwo) / 2; // because floor won’t work with BigInts, subtract the remainder then divide
            //here we are recursively calling the function with half the exponent size, we will square it later but this makes the number smaller/easier to work with
            BigInteger z = modexp(x, yfloored, n);
            if (modtwo == 1) // if the exponent is odd
            {
                //if it's odd we factor out an x to get an even exponent. 
                //z * z is basically squaring, undoing the halving of the exponent earlier, 
                //of course we need to mod everything before passing the result up 
                return (x * ((z * z) % n)) % n;
            }
            else
            {
                //z * z is basically squaring, undoing the halving of the exponent earlier, 
                //of course we need to mod everything before passing the result up 
                return ((z * z) % n);
            }
        }

        // ignoring the randomBigInteger function this would complete in at worst O(n) time
        private BigInteger getRandom(BigInteger max, HashSet<BigInteger> list)
        {
            bool exit = false;
            BigInteger newA = -1; // we are going to change it anyway
            while (!exit)
            {
                newA = randomBigInteger(max); //get a new random number
                exit = !list.Contains(newA); // if we don’t have that number exit the loop, this prevents duplicate A’Bs
            }
            return newA;
        }

        //SOURCE: https://wiki.cs.byu.edu/cs-312/randombigintegers
        BigInteger randomBigInteger(BigInteger N)
        {
            BigInteger result = 0;
            do
            {
                int length = (int)Math.Ceiling(BigInteger.Log(N, 2));
                int numBytes = (int)Math.Ceiling(length / 8.0);
                byte[] data = new byte[numBytes];
                randomizer.NextBytes(data);
                result = new BigInteger(data);
            } while (result >= N || result <= 0);
            return result;
        }
    }
}
